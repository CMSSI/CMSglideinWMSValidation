import sys
import os
import time
import signal

# handle if not int since GLIDEIN_CPUS might be a special word instead like auto
try:
  n_children = int(sys.argv[1])
except ValueError:
  n_children = 1

# 0 for auto, we don't know how many cores so just start 1
if n_children <= 0:
  n_children = 1

wall_time = int(sys.argv[2])

strt_time = time.time()

pids = []

def exit_early(signum, frame):
  for pid in pids:
    os.kill(pid, signal.SIGTERM)
    os.waitpid(pid, 0)
  sys.exit(0)

for i in range(n_children):
  pid = os.fork()

  if pid == 0:
    while True:
      end_time = time.time()
      if end_time - strt_time >= wall_time:
        sys.exit(0)

  else:
    pids.append(pid)

signal.signal(signal.SIGTERM, exit_early)

for pid in pids:
  os.wait()


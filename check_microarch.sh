#!/bin/bash

#################################################################
# CMS Jobs are not supposed to run on nodes that have x86-64-v1 #
#################################################################

# Make sure we can check the architecture
OUT=$(/lib64/ld-linux-x86-64.so.2 --help)
if [ $? -eq 127 ]; then
    echo "'/lib64/ld-linux-x86-64.so.2 --help' exited with code 127 (command not found)."
    # The following lines are used as a fallback. Idea from:
    # https://github.com/htcondor/htcondor/blob/decd0178c35f6a24989c8a5dd12125d12d61cae8/src/condor_sysapi/processor_flags.cpp#L167-L188
    FLAGS="cx16|lahf_lm|popcnt|sse4_1|sse4_2|ssse3|abm|avx|avx2|bmi1|bmi2|f16c|fma|movbe|xsave|avx512bw|avx512cd|avx512dq|avx512f|avx512vl"
    echo "Using fallback method to determine microarchitecture. Checking if CPU has any of these flags using 'lscpu': $FLAGS"
    lscpu | grep Flags | grep -E "$FLAGS" >/dev/null 2>&1
    if [ $? -eq 0 ]; then
        echo "Architecure is at least x86-64-v2 (Fallback method). Continuing."
        exit 0
    fi
    echo "Output of 'lscpu' is:"
    lscpu
    echo "Cannot determine architecture, assuming it is x86-64-v1 and failing the glidein."
    exit 1
fi

# Check if we have at least x86-64-v2
echo "$OUT" | grep x86-64-v2 >/dev/null 2>&1
if [ $? -eq 0 ]; then
    echo "Architecure is at least x86-64-v2. Continuing."
    exit 0
fi

echo "Could not find architecture x86-64-v2. Output of '/lib64/ld-linux-x86-64.so.2 --help' is:"
/lib64/ld-linux-x86-64.so.2 --help

exit 1

def match(job, glidein):
    return (
        # For the memory...
        (
            # GLIDEIN_MaxMemMBs was set to 0 on some "whole-node" entries
            (glidein["attrs"].get("GLIDEIN_MaxMemMBs", 0) == 0) or
            # Memory of the job less than memory of the partitionable slot in the pilot
            (job.get("RequestMemory", 99999) <= glidein["attrs"]["GLIDEIN_MaxMemMBs"])
        ) and 
        # For the OS...
        (
            # The job can run anywhere (OS is "any")
            (job.get("REQUIRED_OS", "any") == "any") or
            # If the pilot is any we can run
            (glidein["attrs"].get("GLIDEIN_REQUIRED_OS", "any")=="any") or
            # If nobosy sets any we check they are the same 
            (job.get("REQUIRED_OS") == glidein["attrs"]["GLIDEIN_REQUIRED_OS"])
        ) and
        # ...
        (
            # GLIDEIN_Job_Min_Time seems never defined in the factory. Not sure what it does...
            (job.get("MaxWallTimeMins", 0)*60) >= glidein["attrs"].get("GLIDEIN_Job_Min_Time", 0)
        ) and
        (
            # The walltime of the jobs has to be less than the walltime of the entry (minus 2 hours): wt < we - 2h
            (job.get("MaxWallTimeMins", 0)+10) < (glidein["attrs"]["GLIDEIN_Max_Walltime"]-glidein["attrs"]["GLIDEIN_Retire_Time_Spread"])/60
        )
    )

# Function to match SiteName OR Entry OR Gatekeeper

def match_site_or_entry_or_gatekeeper(job, glidein):
    return (
        (
            ((glidein["attrs"].get("GLIDEIN_CMSSite") in job["DESIRED_Sites"].split(",")) if (job.has_key("DESIRED_Sites")) else False) or
            ((glidein["attrs"].get("GLIDEIN_Gatekeeper") in job["DESIRED_Gatekeepers"].split(",")) if (job.has_key("DESIRED_Gatekeepers")) else False) or
            ((glidein["attrs"].get("EntryName") in job["DESIRED_Entries"].split(",")) if (job.has_key("DESIRED_Entries")) else False)         
        )

    )

if __name__ == '__main__':
    pass
